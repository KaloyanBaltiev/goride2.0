Kaloyan A. Baltiev

kaloyan.baltiev@gmail.com


                    Documentation for android application GO RIDE 2.0

The application is contained in a folder called go ride 2.0. It can be opened with Android Studio,
and uploaded on an Android phone. The minimum SDK API 17. The application doesn't need any setup.

The application's intention use is for mountain biking, saving routes on the users device but is
not limited to. You can use it as you wish.

On startup it will probably ask you to access  your location. Press allow.The application's
interface is very simple and easy to use. It has tree buttons (now in color) a input view
and a text view. After typing in the map you want to create just press GO RIDE. As soon as it
gets GPS Service it will start printing your currant coordinates. To stop recording press STOP. If
you want to continue recording on the same map type in the name again and press GO RIDE. If you want
to see your maps press SEE. You are now on a second page where you have a list of the names of your
rides. If you want to see it, type in the name and press MAP. If you want to delete it type in
the name and press delete. If you type it wrong it wont let you go. Sorry for the extra typing but we
are programmers after all :) You can also move back and forth the app, open another app, listen to
some music without stop recording.

If you have any questions or need me to send you the APK file please contact me at
kaloyan.baltiev@gmail.com



