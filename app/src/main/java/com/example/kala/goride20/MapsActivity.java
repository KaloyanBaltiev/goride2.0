package com.example.kala.goride20;

/**
 * MapsActivity.java
 *
 * Kaloyan A. Baltiev
 * kaloyan.baltiev@gmail.com
 *
 * Loads google map and plots the user's ride
 */
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Random;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public ArrayList<LatLng> points;
    public Polyline line;
    public static String EXTRA_NAME_RIDE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // get the map's name from LibraryOfMaps.java
        Intent i = getIntent();
        String rideName =i.getStringExtra(EXTRA_NAME_RIDE);

        // get the map's points
        MyDBHandler dbHandler = new MyDBHandler(this, null, null, 7);
        points = dbHandler.getAllLatLng(rideName);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        // set up the map
        mMap = googleMap;
        PolylineOptions options = new PolylineOptions().width(10).color(getRandomColor()).geodesic(true);

        // plot points
        for (LatLng point :points) {
            options.add(point);
        }
        line = mMap.addPolyline(options);

        // move camera
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(0,0), 15)); //removes rotate bugs
        mMap.moveCamera(CameraUpdateFactory.newLatLng(points.get(0)));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    public int getRandomColor(){
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }
}
