package com.example.kala.goride20;
/**
 * GeoPoint.java
 *
 * Kaloyan A. Baltiev
 * kaloyan.baltiev@gmail.com
 *
 * GeoPoint Object
 */

public class GeoPoint {

    private int _id;
    private double _lat;
    private double _lng;
    private String _name;


    // empty constructor
    public GeoPoint(){

    }

    // constructor
    public GeoPoint(double lat, double lng, String name) {
        this._lat = lat;
        this._lng = lng;
        this._name = name;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public double get_lat() {
        return _lat;
    }

    public void set_lat(double _lat) {
        this._lat = _lat;
    }

    public double get_lng() {
        return _lng;
    }

    public void set_lng(double _lng) {
        this._lng = _lng;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }
}

