package com.example.kala.goride20;

/**
 * LibraryOfMapsActivity.java
 *
 * Kaloyan A. Baltiev
 * kaloyan.baltiev@gmail.com
 *
 * Collection od maps each of with can ba loaded or deleted
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class LibraryOfMapsActivity extends AppCompatActivity {

    private ArrayList<String > dbMapName;
    private TextView textView_map;
    private EditText editText_map;
    private MyDBHandler dbHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_of_maps);

        // reference to the objects
        dbHandler = new MyDBHandler(this, null, null, 7);
        textView_map = (TextView) findViewById(R.id.textView_map);
        editText_map = (EditText) findViewById(R.id.editText_map);
        printDatabase();
    }

    // give mapButton functionality
    public void mapButtonClicked(View view) {

        // check input
        String rideName = editText_map.getText().toString();
        if(TextUtils.isEmpty(rideName)) {
            Toast.makeText(getApplicationContext(), "Please enter the name of the ride", Toast.LENGTH_SHORT).show();
            return;
        }
        else if(!checkMaps(rideName)) {
            Toast.makeText(getApplicationContext(), "Please enter the name of the ride", Toast.LENGTH_SHORT).show();
            return;
        }

        // start the MapsActivity and send the maps's name
        else {
            Intent i = new Intent(getApplicationContext(), MapsActivity.class);
            i.putExtra(MapsActivity.EXTRA_NAME_RIDE, rideName);
            startActivity(i);
        }
    }

    // give deleteButton functionality
    public void deleteButtonClicked(View view) {

        // check input
        String rideName = editText_map.getText().toString();
        if(TextUtils.isEmpty(rideName)) {
            Toast.makeText(getApplicationContext(), "Please enter the name of the ride", Toast.LENGTH_SHORT).show();
            return;
        }
        else if(!checkMaps(rideName)) {
            Toast.makeText(getApplicationContext(), "Please enter the name of the ride", Toast.LENGTH_SHORT).show();
            return;
        }

        // delete the map and refresh display
        else {
            dbHandler.deleteGeoPoints(rideName);
            textView_map.setText("Map:");
            printDatabase();
            editText_map.setText("");
        }
    }

    // print the maps
    public void printDatabase() {
        ArrayList<String > dbMapName = dbHandler.databaseMaps();
        for (int i = 0; i < dbMapName.size(); i++) {
            textView_map.append( "\n" + dbMapName.get(i));
        }
    }

    // check if map exists
    public boolean checkMaps(String name) {
        ArrayList<String >dbMapName = dbHandler.databaseMaps();
        if(dbMapName.contains(name)){
            return true;
        }
        return false;
    }
}
